package ru.makproductions.firebasemessenger.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dagger.Component;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.databinding.ColorThemeFragmentBinding;
import ru.makproductions.firebasemessenger.viewmodel.color.theme.ColorThemeViewModel;

public class ColorThemeFragment extends Fragment {

    private ColorThemeViewModel colorThemeViewModel;
    private ColorThemeFragmentBinding binding;

    @Component(modules = {ColorThemeViewModel.class})
    public interface ColorThemeViewmodelCreator {
        ColorThemeViewModel createViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.color_theme_fragment, container, false);
        colorThemeViewModel =
                DaggerColorThemeFragment_ColorThemeViewmodelCreator
                        .builder()
                        .build()
                        .createViewModel();
        colorThemeViewModel.setFragment(this);
        colorThemeViewModel.setBinding(binding);
        colorThemeViewModel.onCreateView();

        binding.setViewmodel(colorThemeViewModel);
        return binding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }
}
