package ru.makproductions.firebasemessenger.view;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import dagger.Component;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.databinding.AuthFragmentBinding;
import ru.makproductions.firebasemessenger.viewmodel.auth.fragment.AuthViewModel;

public class AuthFragment extends Fragment {

    private AuthViewModel viewmodel;

    @Component(modules = {AuthViewModel.class})
    public interface AuthViewmodelCreator {
        AuthViewModel createViewModel();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AuthFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.auth_fragment, container, false);
        viewmodel = DaggerAuthFragment_AuthViewmodelCreator
                .builder()
                .build()
                .createViewModel();
        viewmodel.setBinding(binding);
        viewmodel.setFragment(this);
        viewmodel.onCreateView();
        if (binding == null) {
            throw new NullPointerException("Binding is Null in AuthFragment");
        }
        binding.setViewmodel(viewmodel);
        View rootView = binding.getRoot();
        Log.e("AuthFragment", "onCreateView: " + rootView.getTag());
        final TextInputEditText userNameEditText = binding.userNameEditText;
        final Button submitButton = binding.submitButton;
        submitButton.setOnTouchListener((view, motionEvent) -> viewmodel.onSubmitButtonTouch(view, motionEvent));

        return rootView;
    }
}
