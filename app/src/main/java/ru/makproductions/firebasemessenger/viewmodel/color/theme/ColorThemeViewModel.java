package ru.makproductions.firebasemessenger.viewmodel.color.theme;

import android.databinding.BaseObservable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import ru.makproductions.firebasemessenger.App;
import ru.makproductions.firebasemessenger.databinding.ColorThemeFragmentBinding;

@Module
public class ColorThemeViewModel extends BaseObservable {

    private Fragment fragment;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private ColorThemeFragmentBinding binding;

    @Provides
    public static ColorThemeViewModel getColorThemeViewmodel() {
        return new ColorThemeViewModel();
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setBinding(ColorThemeFragmentBinding binding) {
        this.binding = binding;
    }

    public ColorThemeViewModel() {
    }

    public void onCreateView() {
        if (fragment == null) {
            throw new NullPointerException("ColorThemeFragment is null in viewmodel");
        }
        recyclerView = binding.colorThemeFragmentRecyclerView;
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(App.getInstance());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("blue");
        arrayList.add("orange");
        arrayList.add("green");
        adapter = new ColorThemeAdapter(arrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
    }
}
