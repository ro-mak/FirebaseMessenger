package ru.makproductions.firebasemessenger.viewmodel.photos.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.util.ArrayList;

import ru.makproductions.firebasemessenger.R;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotosRecyclerViewHolder> {
    public static final String TAG = "PhotosAdapter";
    private ArrayList<String> dataSet;
    private ShowPhotoHandler showPhotoHandler;

    public PhotosAdapter(ArrayList<String> dataSet, ShowPhotoHandler showPhotoHandler) {
        this.dataSet = dataSet;
        this.showPhotoHandler = showPhotoHandler;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosRecyclerViewHolder holder, int position) {
        String filePath = dataSet.get(position);
        File file = new File(filePath);
        if (file.exists()) {
            holder.progressBar.setVisibility(View.INVISIBLE);
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            Log.e(TAG, "onBindViewHolder: " + filePath + " " + bitmap);
            if (bitmap != null) {
                ImageView imageView = holder.card.findViewById(R.id.card_photo_image_view);
                imageView.setImageBitmap(bitmap);
            }
        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
        }
        initCardViewOnLongClickListener(holder.card, position);
        initCardViewOnClickListener(holder.card, position);
    }

    @NonNull
    @Override
    public PhotosRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.photo_recycler_item, parent, false);
        return new PhotosRecyclerViewHolder(item);
    }

    private void initCardViewOnLongClickListener(CardView card, int adapterPosition) {
        card.setOnLongClickListener(v -> {
            new File(dataSet.get(adapterPosition)).delete();
            dataSet.remove(adapterPosition);
            notifyDataSetChanged();
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    private void initCardViewOnClickListener(CardView card, int adapterPosition) {
        card.setOnClickListener(v -> showPhotoHandler.onPhotoClick(dataSet.get(adapterPosition)));
    }

    public interface ShowPhotoHandler {
        void onPhotoClick(String photoPath);
    }

    public class PhotosRecyclerViewHolder extends RecyclerView.ViewHolder {
        private CardView card;
        private ProgressBar progressBar;

        public PhotosRecyclerViewHolder(View view) {
            super(view);
            card = view.findViewById(R.id.card_view_photo_recycler_item);
            progressBar = view.findViewById(R.id.photo_progress_bar);
        }
    }

    public ArrayList<String> getDataSet() {
        return dataSet;
    }
}
