package ru.makproductions.firebasemessenger.viewmodel.auth.fragment;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import dagger.Module;
import dagger.Provides;
import ru.makproductions.firebasemessenger.BR;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.databinding.AuthFragmentBinding;
import ru.makproductions.firebasemessenger.model.DataBaseHelper;
import ru.makproductions.firebasemessenger.model.History;
import ru.makproductions.firebasemessenger.model.user.User;
import ru.makproductions.firebasemessenger.model.user.UserBuilder;
import ru.makproductions.firebasemessenger.model.user.UserRepository;
import ru.makproductions.firebasemessenger.model.user.UserUnitOfWork;

@Module
public class AuthViewModel extends BaseObservable implements LifecycleOwner {
    private static final String TAG = "AuthViewModel";
    private Fragment fragment;
    private DataBaseHelper dataBaseHelper;
    private AuthFragmentBinding binding;
    private User user;
    private MutableLiveData<User> userMLD;
    private UserRepository userRepository;
    private LifecycleRegistry lifecycleRegistry;
    private Animation submitButtonAnimation;

    @Provides
    static AuthViewModel getAuthViewmodel() {
        return new AuthViewModel();
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setBinding(AuthFragmentBinding binding) {
        this.binding = binding;
    }

    public AuthViewModel() {
        dataBaseHelper = new DataBaseHelper();
    }


    public void onCreateView() {
        Context context = fragment.getContext();
        submitButtonAnimation = AnimationUtils.loadAnimation(context, R.anim.button_alpha);
        lifecycleRegistry = new LifecycleRegistry(this);
        lifecycleRegistry.markState(Lifecycle.State.CREATED);
        UserUnitOfWork.init(context);
        userMLD = new MutableLiveData<User>();
        userMLD.setValue(new UserBuilder()
                .withName("Petya")
                .withSurname("Fedorov")
                .build());
        userRepository = UserUnitOfWork.getInstance();
        userRepository.addUser(userMLD);
        UserUnitOfWork.getInstance().commit();
        user = userRepository.getUser(1).getValue();


        final Observer<User> userObserver = new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                Log.e("MainViewModel", "onChange: " + user.getName());
                AuthViewModel.this.user = user;
            }
        };
        userMLD.observe(this, userObserver);
    }

    public void onResume() {
        Log.e("MainViewModel", "onResume: " + userMLD.getValue().getName());
        lifecycleRegistry.markState(Lifecycle.State.RESUMED);
        if (userMLD != null && userMLD.getValue() != null) {
            User user1 = userMLD.getValue();
            user1.setName(dataBaseHelper.load());
            userMLD.setValue(user1);
        }
    }

    @Bindable
    public Editable getMessage() {
        return user.getMessage();
    }

    public void setMessage(Editable message) {
        user.setMessage(message);
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public History getHistory() {
        return user.getHistory();
    }

    public void setHistory(History history) {
        user.setHistory(history);
        notifyPropertyChanged(BR.history);
    }

    @Bindable
    public String getName() {
        return user.getName();
    }

    public void setName(String name) {
        user.setName(name);
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getSurname() {
        return user.getSurname();
    }

    public void setSurname(String surname) {
        user.setSurname(surname);
        notifyPropertyChanged(BR.surname);
    }

    @Bindable
    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
        notifyPropertyChanged(BR.status);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }

    public boolean onSubmitButtonTouch(View view, MotionEvent motionEvent) {
        Log.e(TAG, "onTouch: ");
        int action = motionEvent.getAction();
        Log.e(TAG, "onTouch: " + action);
        if (action == MotionEvent.ACTION_DOWN) {
            Resources resources = view.getResources();
            view.startAnimation(submitButtonAnimation);
            ViewCompat.setElevation(view,
                    resources.getDimension(R.dimen.submit_button_translation_z));
            return view.performClick();
        } else if (action == MotionEvent.ACTION_UP) {
            setSurname(binding.userNameEditText.getText().toString());
            setMessage(binding.userNameEditText.getText());
            Log.e("MAVM", "onClick: " + binding.userNameEditText.getText());
            ViewCompat.setElevation(view,
                    0);
            return view.performClick();
        } else if (action == MotionEvent.ACTION_CANCEL) {
            ViewCompat.setElevation(view,
                    0);
            return view.performClick();
        }
        return false;
    }
}
