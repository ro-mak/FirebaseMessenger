package ru.makproductions.firebasemessenger.view;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.makproductions.firebasemessenger.Prefs;
import ru.makproductions.firebasemessenger.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    @BindView(R.id.main_drawer_layout) DrawerLayout mainDrawerLayout;
    @BindView(R.id.main_toolbar) Toolbar toolbar;
    @BindView(R.id.main_nav_view) NavigationView navigationView;
    private Fragment fragment;
    private FragmentManager fragmentManager
            = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Prefs.getInstance().getTheme());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        initUi();
    }

    private void initUi() {
        initActionBar();
        initNavView();
    }

    private void initNavView() {
        navigationView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.main_drawer_menu_item) {
                if (fragment != null) {
                    detachFragment();
                }
            } else if (id == R.id.choose_color_theme_drawer_menu_item) {
                fragment = new ColorThemeFragment();
                changeFragment(fragment, R.id.fragment_container);
                Log.e(TAG, "onNavigationItemSelected: ColorTheme");
            }
            mainDrawerLayout.closeDrawers();
            return true;
        });
    }

    private void initActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar == null) {
            throw new NullPointerException("Actionbar is null in OnCreate MainActivity");
        }
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
    }

    @Override
    protected void onPause() {
        if (fragment != null) {
            detachFragment();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: " + fragment);
        super.onResume();
    }

    private void changeFragment(Fragment fragment, int layoutId) {
        FragmentTransaction fragmentTransaction
                = fragmentManager.beginTransaction();
        fragmentTransaction.add(layoutId, fragment);
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    private void detachFragment() {
        fragmentManager.beginTransaction().detach(fragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            mainDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
