package ru.makproductions.firebasemessenger.viewmodel.color.theme;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import ru.makproductions.firebasemessenger.App;
import ru.makproductions.firebasemessenger.Prefs;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.view.MainActivity;

public class ColorThemeAdapter extends
        RecyclerView.Adapter<ColorThemeAdapter.ColorThemeRecyclerViewHolder> {
    private ArrayList<String> dataSet;

    public ColorThemeAdapter(ArrayList<String> dataSet) {
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public ColorThemeRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.color_theme_recycler_item, parent, false);
        return new ColorThemeAdapter.ColorThemeRecyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorThemeAdapter.ColorThemeRecyclerViewHolder holder, int position) {
        holder.item.setText(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class ColorThemeRecyclerViewHolder extends RecyclerView.ViewHolder {
        private Button item;

        public ColorThemeRecyclerViewHolder(View view) {
            super(view);
            item = view.findViewById(R.id.button_color_theme_recycler_item);
            item.setOnClickListener(v -> {
                String buttonClicked = ((Button) v).getText().toString();
                if ((buttonClicked.equals("green"))) {
                    Prefs.getInstance().setThemeId(R.style.GreenTheme);
                } else if (buttonClicked.equals("blue")) {
                    Prefs.getInstance().setThemeId(R.style.BlueTheme);
                } else if (buttonClicked.equals("orange")) {
                    Prefs.getInstance().setThemeId(R.style.OrangeTheme);
                }
                startMainActivity();

            });
        }
    }

    private void startMainActivity() {
        Context context = App.getInstance();
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
