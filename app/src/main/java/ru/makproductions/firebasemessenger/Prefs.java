package ru.makproductions.firebasemessenger;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {
    private static volatile Prefs instance;
    private static String MAIN_SHARED_PREFERENCES = "main";
    private static String THEME_PREFS_NAME = "theme";
    SharedPreferences sharedPreferences;

    private Prefs() {
        sharedPreferences = App.getInstance().getSharedPreferences(MAIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    public int getTheme() {
        return sharedPreferences.getInt(THEME_PREFS_NAME, R.style.AppTheme);
    }

    public void setThemeId(int id) {
        sharedPreferences.edit().putInt(THEME_PREFS_NAME, id).apply();
    }

    public static Prefs getInstance() {
        Prefs localInstance = instance;
        if (localInstance == null) {
            synchronized (Prefs.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Prefs();
                }
            }
        }
        return localInstance;
    }
}
