package ru.makproductions.firebasemessenger.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;

import dagger.Component;
import ru.makproductions.firebasemessenger.Prefs;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.databinding.ShowPhotoActivityBinding;
import ru.makproductions.firebasemessenger.viewmodel.show.photo.activity.ShowPhotoViewModel;

public class ShowPhotoActivity extends AppCompatActivity {
    private static final String TAG = "ShowPhotoActivity";
    private ShowPhotoViewModel viewModel;
    private String photoPath;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(Prefs.getInstance().getTheme());
        super.onCreate(savedInstanceState);
        ShowPhotoActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.show_photo_activity);
        viewModel = DaggerShowPhotoActivity_ShowPhotoViewModelCreator
                .builder()
                .build()
                .createViewModel();
        binding.setViewmodel(viewModel);
        Toolbar toolbar = binding.showPhotoToolbar;
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            photoPath = intent.getExtras().getString("photoPath");
            loadPhoto(binding, photoPath);
        }

    }

    private void loadPhoto(ShowPhotoActivityBinding binding, String photoPath) {
        File file = new File(photoPath);
        if (file.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(photoPath);
            Log.e(TAG, "loadPhoto: " + photoPath + " " + bitmap);
            if (bitmap != null) {
                ImageView imageView = binding.showPhotoImageView;
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    @Component(modules = {ShowPhotoViewModel.class})
    public interface ShowPhotoViewModelCreator {
        ShowPhotoViewModel createViewModel();
    }
}
