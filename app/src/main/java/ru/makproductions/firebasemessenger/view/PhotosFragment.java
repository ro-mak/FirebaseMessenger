package ru.makproductions.firebasemessenger.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dagger.Component;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.databinding.PhotosFragmentBinding;
import ru.makproductions.firebasemessenger.viewmodel.photos.fragment.PhotosViewModel;

public class PhotosFragment extends Fragment {
    private PhotosViewModel viewModel;
    private static String TAG = "PhotosFragment";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView: ");
        PhotosFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.photos_fragment, container, false);
        viewModel = DaggerPhotosFragment_PhotosViewModelCreator
                .builder()
                .build()
                .createViewModel();
        viewModel.setBinding(binding);
        viewModel.setFragment(this);
        if (binding == null) {
            throw new NullPointerException("Binding is Null in PhotosFragment");
        }
        binding.setViewmodel(viewModel);

        View rootView = binding.getRoot();
        Log.e(TAG, "onCreateView: " + rootView.getTag());
        viewModel.onCreateView();
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Component(modules = {PhotosViewModel.class})
    public interface PhotosViewModelCreator {
        PhotosViewModel createViewModel();
    }
}
