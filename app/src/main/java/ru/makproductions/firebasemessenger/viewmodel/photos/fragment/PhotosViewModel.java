package ru.makproductions.firebasemessenger.viewmodel.photos.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.BaseObservable;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import dagger.Module;
import dagger.Provides;
import ru.makproductions.firebasemessenger.App;
import ru.makproductions.firebasemessenger.R;
import ru.makproductions.firebasemessenger.databinding.PhotosFragmentBinding;
import ru.makproductions.firebasemessenger.view.ShowPhotoActivity;

import static android.app.Activity.RESULT_OK;

@Module
public class PhotosViewModel extends BaseObservable implements PhotosAdapter.ShowPhotoHandler {
    private static final int NUMBER_OF_COLUMNS = 3;
    public static final String TAG = "PhotosViewModel";
    private static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 324;
    private Fragment fragment;
    private PhotosFragmentBinding binding;
    private RecyclerView recyclerView;
    private PhotosAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private final int requestImageCapture = 1;
    private static File dcimDirectory;
    private ArrayList<String> photosList;

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setBinding(PhotosFragmentBinding binding) {
        this.binding = binding;
    }

    @Provides
    public static PhotosViewModel getMainActivityFragmentViewModel() {
        return new PhotosViewModel();
    }

    public PhotosViewModel() {
        dcimDirectory = App.getInstance().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    private void initDefaultPhotosList() {
        Log.e(TAG, "InitPhotosList: start");
        checkPermissionsToReadAndWriteExternalStorage();
        if (!isExternalStorageReadable()) {
            Snackbar.make(binding.photoFragmentLayout,
                    R.string.external_storage_is_not_readable, Snackbar.LENGTH_SHORT).show();
            Log.e(TAG, "InitPhotosList: " + "External storage is not readable");
        }
        photosList = new ArrayList<>();

        Log.e(TAG, "InitPhotosList: " + dcimDirectory.getAbsolutePath());
        String[] fileList = dcimDirectory.list();
        Log.e(TAG, "InitPhotosList: " + "fileList = " + Arrays.toString(fileList));
        if (fileList != null) {
            for (int i = 0; i < fileList.length; i++) {
                photosList.add(dcimDirectory.getAbsolutePath() + "/" + fileList[i]);
                Log.e(TAG, "InitPhotosList: " + dcimDirectory.getAbsolutePath() + " " + fileList[i]);
            }
        }
        Log.e(TAG, "InitPhotosList: end");
    }

    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    public void onCreateView() {
        recyclerView = binding.photosRecyclerView;
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(fragment.getContext(), NUMBER_OF_COLUMNS);
        initDefaultPhotosList();
        adapter = new PhotosAdapter(photosList, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        setAddPhotoButtonListener();
    }

    private void setAddPhotoButtonListener() {
        FloatingActionButton addPhotoButton = binding.addPhotoButton;
        addPhotoButton.setOnClickListener(v -> {
            Activity activity = fragment.getActivity();
            if (activity == null) {
                throw new NullPointerException("Activity is null in PhotosFABonClickListener");
            }
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                fragment.startActivityForResult(takePictureIntent, requestImageCapture);
            }
        });
    }

    private File createImageFile() {
        StringBuilder photoName = new StringBuilder();
        photoName.append("Photo-");
        photoName.append(Calendar.getInstance().getTime().getTime());
        photoName.append(".jpg");
        return new File(dcimDirectory, photoName.toString());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult: ");
        if (requestCode == requestImageCapture && resultCode == RESULT_OK) {
            Log.e(TAG, "onActivityResult: ImageCapture OK");
            checkPermissionsToReadAndWriteExternalStorage();
            Bundle extras = data.getExtras();
            Log.e(TAG, "onActivityResult: Extras = " + extras);
            if (extras != null) {
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                Log.e(TAG, "onActivityResult: Bitmap = " + imageBitmap);
                if (imageBitmap != null) {
                    Activity activity = fragment.getActivity();
                    if (activity == null) {
                        throw new NullPointerException("Activity is null in PhotosFABonClickListener");
                    }
                    File imageFile = createImageFile();
                    if (dcimDirectory != null && isExternalStorageWritable()) {
                        Log.e(TAG, "onActivityResult: Saving to storage");
                        FileOutputStream outputStream;
                        try {
                            Log.e(TAG, "onActivityResult: ImagePath = " + imageFile);
                            outputStream = new FileOutputStream(imageFile.getAbsolutePath());
                            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                            outputStream.flush();
                            outputStream.close();
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
                        }
                    }
                    addPhoto(imageFile.getAbsolutePath());
                    Snackbar.make(binding.photoFragmentLayout,
                            R.string.photo_added, Snackbar.LENGTH_SHORT).show();
                }
            }

        }
    }

    private void addPhoto(String path) {
        Log.e(TAG, "addPhoto: ");
        boolean added = false;
        ArrayList<String> dataSet = adapter.getDataSet();
        for (int i = 0; i < dataSet.size(); i++) {
            if (added) {
                return;
            }
            if (dataSet.get(i).isEmpty()) {
                dataSet.set(i, path);
                added = true;
                adapter.notifyDataSetChanged();
            }
        }
        if (!added) {
            dataSet.add(path);
            adapter.notifyDataSetChanged();
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void checkPermissionsToReadAndWriteExternalStorage() {
        if (ContextCompat.checkSelfPermission(App.getInstance(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (fragment == null || fragment.getActivity() == null) {
                throw new NullPointerException(TAG + " Fragment or fragment.getActivity is null in checkPermissions");
            }
            Activity activity = fragment.getActivity();
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(binding.photoFragmentLayout, R.string.permissions_to_external_storage_explanation, Snackbar.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onPhotoClick(String photoPath) {
        Intent intent = new Intent(fragment.getActivity(), ShowPhotoActivity.class);
        intent.putExtra("photoPath", photoPath);
        fragment.startActivity(intent);
    }
}
