package ru.makproductions.firebasemessenger.viewmodel.show.photo.activity;

import android.databinding.BaseObservable;

import dagger.Module;
import dagger.Provides;

@Module
public class ShowPhotoViewModel extends BaseObservable {
    @Provides
    public ShowPhotoViewModel getShowPhotoViewModel() {
        return new ShowPhotoViewModel();
    }
}
